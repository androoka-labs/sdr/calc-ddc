


function cic_plot (N,R,Fs=125e6,freqn=0:.001:1)
  printf("cic dec=%d, output = %.1fkHz (pfir=%.1f kSPS)\n",R,(Fs/R)/1000,(Fs/R)/4000);
  Hdb = 20*log10(cic_response(N,R,freqn));
#  plot((Fs/R)*freqn/1e3,Hdb);

  plot(freqn*(Fs/R)/2,Hdb);
  hold on;

#  freqn = 0:.001:1;
#  plot((1-freqn)*(Fs/R)/2,20*log10(cic_response(N,R,freqn)));

  hold on;
  freqn = 1:.001:2;
  Hdb = 20*log10(cic_response(N,R,freqn));
  plot((2-freqn)*(Fs/R)/2,Hdb);

  freqn = 2:.001:3;
  Hdb = 20*log10(cic_response(N,R,freqn));
  plot((freqn-2)*(Fs/R)/2,Hdb);

  % freqn = 3:.001:4;
  % Hdb = 20*log10(cic_response(N,R,freqn));
  % plot((4-freqn)*(Fs/R)/2,Hdb);


  hold off;

  set(gca,'ylim',[-220,0])
  set(gca,'xtick',[.2 .25 .3 .5 .75 .8]*(Fs/R)/2)
  set(gca,'xgrid','on')

  x = [ .2 .25 .45 .5 .55 1.80];
  ytick = 20*log10(cic_response(N,R,x));
  set(gca,'ytick',[ytick, -200:10:-80, -20:1:0]);
  set(gca,'ygrid','on')

endfunction

