

#Fs =125e6;
#R = 250;
#target_rejection = -120; # db

Fs = input("Sampling Frequency in MHz [120] > ");
if isempty(Fs)
  Fs = 120e6;
else
  Fs = Fs * 1e6;
endif

R = input("CIC Decimation Rate change [800] > ");
if isempty(R)
  R = 800;
endif

target_rejection = input("Target Rejection [-105] > ");
if isempty(target_rejection)
  target_rejection = -105;
endif
