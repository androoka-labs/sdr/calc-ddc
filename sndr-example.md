
<pre>
Sampling Frequency in MHz [120] > 120
CIC Decimation Rate change [800] > 750
Target Rejection [-105] > -110
</pre> 
<pre>
using N=6 stages, image rejection = -114.5dB  
passband droop=3.5dB
cic dec=750, output = 160.0kHz (pfir=40.0 kSPS)
N = 6,   R = 750,   M = 1,   Bin = 24,   Bout = 32  
Num of Bits Growth Due To CIC Filter Gain = 58  
Num of Accumulator Bits With No Truncation = 82  
</pre>

| Stage(j)|   Bj  | Accum<br>width  
|-|-|-
|   1	 |  -4	  |   86  
|   2	 |   5	  |   77  
|   3	 |  14	  |   68  
|   4	 |  23	  |   59  
|   5	 |  31	  |   51  
|||
|   6	 |  39	  |   43  
|   7	 |  43	  |   39  
|   8	 |  44	  |   38  
|   9	 |  45	  |   37  
|  10	 |  46	  |   36  
|  11	 |  46	  |   36  
|  12	 |  47	  |   35  
|||
|  13	 |  50	  |   32 <br>(final truncation)  

<pre>
integrator luts=384  
target_max_cfir_var_db =  0.10000  
cfir requires 77 taps for 0.10 ripple
 22+ceil(77/2)=61 *2IQ= 122 MMAC/S, 
 120e6/122equals 983kSPS samples/s (I+Q) @ 120MHz
 at 200MHz, max bw=1.63MSPS
</pre>