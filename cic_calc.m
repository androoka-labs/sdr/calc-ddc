# design the cic 
pkg load signal

close all
clear all

# load vars from the a file
loadvars

N = 0;

cutoff = 0.8/4;

hold off;

do 
  N = N + 1;
  passband_droop = 20*log10(cic_response(N,R,cutoff));
  rejection = 20*log10(cic_response(N,R,2-cutoff));
  rejection = rejection - passband_droop;
until rejection < target_rejection;
printf("using N=%d stages, image rejection = %.1fdB passband droop=%.1fdB\n",N, rejection,-passband_droop);

figure(1)
cic_plot(N,R,Fs=Fs,freqn=0:.001:1)


M=1;
Bin=32;
Bout=32;

warning('off','all');

cic_prune

edge = 0.24;
nctaps = 21;
additional_gain = 0;
target_max_cfir_var_db = 0.1

max_cfir_var_db = target_max_cfir_var_db;

function [j,val] = find_closest(f,fa,va)
  j=0;
  l = size(fa)(2);
  j = 1;
  while(f>fa(j))
    j = j + 1;
  endwhile
  val = va(j);
endfunction

function taps = quantise(taps,nbits)
  mct = max(taps);
  taps = taps / mct;
  taps = round(taps*power(2,nbits))/power(2,nbits);
  taps = taps * mct;
endfunction


cfir_f1 = 0:(edge/128):edge;
nf1 = size(cfir_f1)(2);

cfir_tweak_dB = cfir_f1(1:(nf1-1))*0;

do

  cfir_a = 20*log10(cic_response(N,R,cfir_f1));
  % flatten end
  cfir_a(end-1) = cfir_a(end-2);
  cfir_a(end) = cfir_a(end-1);

  % inverse
  % cfir_a = min(cfir_a)-cfir_a;
  cfir_a = min(cfir_a)-cfir_a;
  cfir_a = power(10,(cfir_a/20));
  cfir_a = cfir_a;% - max(cfir_a);

  % cfir decimates by 2 and k1 is the point
  % where the primary cic response will fold back
  % into the pfir input passband
  k1 = 20*log10(cic_response(N,R,0.8))+additional_gain;

  % we want to suppress to the level of the target rejection
  if(k1 < target_rejection)
    k2 = target_rejection;
  else
    k2 = target_rejection-k1;
  endif

  p = power(10,k2/20);

  p1 = power(10,-20/20);
  p2 = power(10,-20/20);

  cfir_f = [cfir_f1,[.32 .4 .55 .8 1]];
  cfir_a = [cfir_a,[p1 p1 p1 p p]];


  ctaps = fir2(nctaps,cfir_f,cfir_a);
  ctaps = quantise(ctaps,32);

  cfir_resp = (20*log10(abs(fft(ctaps,size(freqn)(2)*2))))(1:1001);
  cic_r1 = 20*log10(cic_response(N,R,freqn));
  cic_r = cic_r1 + cfir_resp;

  for i = 1:size(cfir_f1)(2)
    a = cfir_f1(i);
    [x,y] = find_closest(a,freqn,cic_r);
    cfir_tweak_dB(i)=y;
  endfor

  cfir_tweak_dB = cfir_tweak_dB - max(cfir_tweak_dB);
  % cfir_tweak_dB(end-1) = cfir_tweak_dB(end-2);
  % cfir_tweak_dB(end) = cfir_tweak_dB(end-1);

  cfir_a(1:nf1) = cfir_a(1:nf1) .* power(10,cfir_tweak_dB/20);

  % calculate tweaked 
  ctaps = fir2(nctaps,cfir_f,cfir_a);
  ctaps = quantise(ctaps,32);

  cfir_resp = (20*log10(abs(fft(ctaps,size(freqn)(2)*2))))(1:1001);
  cic_r1 = 20*log10(cic_response(N,R,freqn));
  cic_r = cic_r1 + cfir_resp;





  cfir_passband_resp = cic_r;
  passband_variation =  max(cfir_passband_resp(1:200))-min(cfir_passband_resp(1:200));


  image_resp = max(cfir_passband_resp(800:1001));
  if (image_resp > (target_rejection))
    % current_k2 = 20*log10(cfir_a(end-1));
    % new_k2 = current_k2 - (image_resp - target_rejection) - additional_gain;
    % cfir_a(end-1) = power(10,new_k2/20);
    passband_variation = 100; % force recalc
    additional_gain = additional_gain + 0.5;
  else
    nctaps = nctaps + 2;
  % elseif passband_variation >= max_cfir_var_db
  %   edge = edge + 0.01;
  endif

  if nctaps>100
    max_cfir_var_db = target_max_cfir_var_db + 0.05*(nctaps-100)
  endif


until (passband_variation < max_cfir_var_db)

printf("cfir requires %d taps for %.2f ripple\n",nctaps,max_cfir_var_db);

figure(2)
plot(cfir_passband_resp(1:201))

figure(1)

hold on;
plot(cfir_f*(Fs/R)/2,20*log10(cfir_a),"-");
plot(freqn*(Fs/R)/2,cic_r,'.');
plot((1-freqn)*(Fs/R)/2,cic_r,'--');

cfir_cr = cfir_resp + cic_r;


hold off;


legend('cic','cic image2','cic image3','cfir targ','cfir out');

#pause
#set(gca,'ylim',[-10 0])
#set(gca,'xlim',[0 62500])
#plot(freqn*(Fs/R)/2,cfir_resp);



% figure(2)
% plot(1-cfir_b)

#CIC_Word_Truncation