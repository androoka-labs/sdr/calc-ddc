


function h = cic_response (N,R,freqn=0:.001:4)
  #fn - normalised frequency
  w = 2 * pi * (freqn/R);
  z = e.^(1j*w);
  h = abs((1 / R * (1 - z .^ (-R)) ./ (1 - z .^ (-1))) .^ N);
  for i = 1:size(w)(2)
    if (w(i)==0)
      h(i)=1;
    endif
  endfor
endfunction

